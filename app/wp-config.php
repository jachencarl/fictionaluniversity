<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

if (file_exists(dirname(__FILE__) . '/local.php')) {
// Local Database Setting

define('DB_NAME', 'dbname');
define('DB_USER', 'dbuser');
define('DB_PASSWORD', '123');
define('DB_HOST', 'localhost');

} else {

// Hosting Metanet Settings	
define('DB_NAME', 'widen-onlinech_university');
define('DB_USER', 'jachencarl');
define('DB_PASSWORD', 'jc254339');
define('DB_HOST', 'localhost');

}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y17tO+#q*5q!N`*(v0M]+5LLB,!;Ic+6 EAsBw0KE@RUQnyn%X[ZYK|aKoE,maR^');
define('SECURE_AUTH_KEY',  't]RM<~KjN@}h#nP;Maso|<[ixAn7y4Jc(9V>5Hwf[7~Tv27IT{[0!GtJuXpKaop<');
define('LOGGED_IN_KEY',    'XXG}{f^m{$7Sn>!5nDx59Pdxn3Yp&c~VR,0R/%.7)y-}?0~>Ha}5Jkl.}{f@NE, ');
define('NONCE_KEY',        ')c)EZ4!,P+CrED1]qCvMiTs}n-%yp:*4AM/|Kn=^;A_A]e-#<A[/ifbE~<M7JTZk');
define('AUTH_SALT',        'OGDsk`L|?k02@upq&46.%zUaQ~.Vhe/nR!comuDKFIs-{(~Ga]%19xU-gQLjSeY~');
define('SECURE_AUTH_SALT', 'S:Q}BH1B&<,/Tx#Qy(-0f$.aaJH|s)Ig_5J=S!JgA`wx&{G}h<QE@[P[|v9BJ{GZ');
define('LOGGED_IN_SALT',   'v~w_Ai=&.4zc%kgCc&q>N>VfXgUYrupw]|q*s//TD[xpFl>M_QgMu@{WrAh%R90A');
define('NONCE_SALT',       'aA(mu;I^`_Pn9_)JnpY]`F& ;+WfSN^5vpBnJ<1?x4-*cBy9OfvejW{m[JuP*QA;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
